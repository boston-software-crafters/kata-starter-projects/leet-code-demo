# leet-code-demo

A demo of how to use leet-code TDDing

## Demo Case: Setting Things Up For TDD

### Starting

pick a problem.  I picked a medium problem at random

![img.gif](docs-images/leet-code-main-page.gif)


### The Demo Problem We Are Using

https://leetcode.com/problems/add-two-numbers/

select the python3 tab for language and copy that code to your file.
**The `ListNode` code should be un-commneted and it can _not_ be changed**

![img.png](docs-images/language-tab.png)

read the description

![img.png](docs-images/description-tab.png)

### Begin implementation

set up a test runner.

We want test cases that use list inputs and outputs, like the examples.

We will need:
1. linked_list_to_list
2. list_to_linked_list
3. a convenient way to run tests?  

#### implementing what became a recursive function

test empty
```python
def linked_list_to_list(node: Optional[ListNode]) -> List[int]:
    return []
```

test with single node
```python
def linked_list_to_list(node: Optional[ListNode]) -> List[int]:
    if not node:
        return []
    return [node.val]
```

test multi node
```python
def linked_list_to_list(node: Optional[ListNode]) -> List[int]:
    if not node:
        return []
    return [node.val] + TestRunner.linked_list_to_list(node.next)
```
    
#### hit a speed bump

```python

def test_multi_value_list(self):
    input_list = [3, 4, 5]
    result = TestRunner.list_to_linked_list(input_list)
    assert TestRunner.linked_list_to_list(result) == input_list
```
here's the result:
```
E       assert [3] == [4, 5]
```
The input list is getting mutated, first make it fail correctly
```python

def list_to_linked_list(int_list: List[int]) -> Optional[ListNode]:
    use_list = int_list[:]  # fixed!
```

### The first test

```python

class TestSolution:
    def test_single_values_that_add_to_less_than_ten(self):
        input_list_one = [1]
        input_list_two = [2]
        expected_output_list = [3]

        first = TestRunner.list_to_linked_list(input_list_one)
        second = TestRunner.list_to_linked_list(input_list_two)
        node_result = Solution.addTwoNumbers(first, second)
        actual = TestRunner.linked_list_to_list(node_result)
        assert actual == expected_output_list

```
 
the test is failing incorrectly! 
`E  TypeError: addTwoNumbers() missing 1 required positional argument: 'l2'
`
the fix:
```python
node_result = Solution().addTwoNumbers(first, second)
```

and now we get: `E   assert [] == [3]`

### After First Test Passes

I set up my second test and it passed.  Then I had code duplication in my test cases.
In this case (and some will argue with me here) I wanted to make sure that refactoring my
tests would not affect either passing or failing tests.  So I reverted my solution to get a
correct pass and the following correct failure:

```
Expected :[3, 1]
Actual   :[3]
```

_then_ I refactored the following:

```python

def test_single_values_that_add_to_less_than_ten(self):
    input_list_one = [1]
    input_list_two = [2]
    expected_output_list = [3]

    first = TestRunner.list_to_linked_list(input_list_one)
    second = TestRunner.list_to_linked_list(input_list_two)
    node_result = Solution().addTwoNumbers(first, second)
    actual = TestRunner.linked_list_to_list(node_result)
    assert actual == expected_output_list
```

I extracted a new method:
```python

def assert_add_two_numbers(self, input_list_one, input_list_two, expected_output_list):
    first = TestRunner.list_to_linked_list(input_list_one)
    second = TestRunner.list_to_linked_list(input_list_two)
    node_result = Solution().addTwoNumbers(first, second)
    actual = TestRunner.linked_list_to_list(node_result)
    assert actual == expected_output_list
```

and confirmed my passing and failing tests

Then put back my solution

## Conclusion

We are now set up (using TDD) to develop a solution (using TDD).  We can paste in the solution
object when we are done and they have many handy test cases for us to run.

After putting my solution into leet code, they gave me an error, pointing to a possible next 
test case

![img.png](docs-images/failing-test-case.png)

### testing strategy I might use

- [1, 2], [3, 4] -> [4, 6]
- [4, 5], [4, 6] -> [8, 1, 1]
- [5, 4], [6, 4] -> [1, 9]
- [5, 5], [6, 4] -> [1, 0, 1]
- [4, 5], [1] -> [5, 5]
- [9, 5], [1] -> [0, 6]
- [9, 9, 9], [1] -> [0, 0, 0, 1]
- negative numbers
- commutative

### other refactorings

- TestRunner seems un-necessary. just have some helper functions
- Move the assert function out of the class
- Rename many things