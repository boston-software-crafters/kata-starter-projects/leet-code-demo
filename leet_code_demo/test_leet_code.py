from typing import List, Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        new_val = l1.val + l2.val
        remainder, this_node = divmod(new_val, 10)
        result = ListNode(this_node)
        if remainder:
            result.next = ListNode(remainder)
        return result


class TestRunner:
    @staticmethod
    def linked_list_to_list(node: Optional[ListNode]) -> List[int]:
        if not node:
            return []
        return [node.val] + TestRunner.linked_list_to_list(node.next)

    @staticmethod
    def list_to_linked_list(int_list: List[int]) -> Optional[ListNode]:
        use_list = int_list[:]
        if not use_list:
            return None
        result = ListNode(val=use_list.pop(0))
        result.next = TestRunner.list_to_linked_list(use_list)
        return result


"""
Step 1.
set up our testing apparatus to use their test cases.

They pass in lists and get out lists.  We should do the same.
"""


class TestLinkedListToList:
    def test_no_list(self):
        assert TestRunner.linked_list_to_list(None) == []

    def test_list_with_one_node(self):
        assert TestRunner.linked_list_to_list(ListNode(5)) == [5]

    def test_list_with_multiple_nodes(self):
        first_node = ListNode(5)
        second_node = ListNode(3)
        third_node = ListNode(2)
        first_node.next = second_node
        second_node.next = third_node
        assert TestRunner.linked_list_to_list(first_node) == [5, 3, 2]


class TestListToLinkedList:
    def test_empty_list(self):
        result = TestRunner.list_to_linked_list([])
        assert result is None
        assert TestRunner.linked_list_to_list(result) == []

    def test_single_value_list(self):
        result = TestRunner.list_to_linked_list([3])
        assert TestRunner.linked_list_to_list(result) == [3]

    def test_multi_value_list(self):
        input_list = [3, 4, 5]
        result = TestRunner.list_to_linked_list(input_list)
        assert TestRunner.linked_list_to_list(result) == input_list


class TestSolution:
    def assert_add_two_numbers(
        self, input_list_one, input_list_two, expected_output_list
    ):
        first = TestRunner.list_to_linked_list(input_list_one)
        second = TestRunner.list_to_linked_list(input_list_two)
        node_result = Solution().addTwoNumbers(first, second)
        actual = TestRunner.linked_list_to_list(node_result)
        assert actual == expected_output_list

    def test_single_values_that_add_to_less_than_ten(self):
        input_list_one = [1]
        input_list_two = [2]
        expected_output_list = [3]

        self.assert_add_two_numbers(
            input_list_one, input_list_two, expected_output_list
        )

    def test_single_values_that_add_to_more_than_ten(self):
        input_list_one = [6]
        input_list_two = [7]
        expected_output_list = [3, 1]

        self.assert_add_two_numbers(
            input_list_one, input_list_two, expected_output_list
        )
